package com.easy.easytable.dto;

import lombok.Getter;
import lombok.Setter;

@Getter@Setter
public class ClientCreationCompteDto extends ClientDto{
    private String motDePasse;
}
