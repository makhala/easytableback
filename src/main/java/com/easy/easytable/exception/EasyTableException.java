package com.easy.easytable.exception;

public class EasyTableException extends  Exception{
    public EasyTableException(){super();}
    public EasyTableException(String message){super();}
    public EasyTableException(String message, Throwable cause){super();}
}
