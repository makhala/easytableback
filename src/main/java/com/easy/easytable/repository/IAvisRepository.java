package com.easy.easytable.repository;

import com.easy.easytable.entity.AvisEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IAvisRepository extends JpaRepository<AvisEntity, Integer> {
}
